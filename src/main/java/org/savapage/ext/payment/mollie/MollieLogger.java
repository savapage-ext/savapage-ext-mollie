/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie;

import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;

import org.savapage.core.json.JsonAbstractBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Datraverse B.V.
 * @since 0.9.9
 */
public class MollieLogger {

    /**
     * .
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(MollieLogger.class);

    /**
     *
     * @return
     */
    public static Logger getLogger() {
        return LOGGER;
    }

    public static boolean isEnabled() {
        return LOGGER.isInfoEnabled();
    }

    public static void logCallback(final Map<String, String[]> parameterMap) {

        if (!isEnabled()) {
            return;
        }

        final StringBuilder builder = new StringBuilder(128);

        for (final Entry<String, String[]> entry : parameterMap.entrySet()) {
            builder.append("\nParm [").append(entry.getKey()).append("] =");
            for (final String value : entry.getValue()) {
                builder.append(" [").append(value).append("]");
            }
        }

        LOGGER.info(String.format(
                "|________________ Callback ________________|\n%s\n",
                builder.toString()));
    }

    public static void logRequest(final String json, final URI uri) {
        if (isEnabled()) {
            LOGGER.info(String.format(
                    "|________________ %s ________________|\n%s\n",
                    uri.toString(), json));
        }
    }

    public static void logResponse(final String json, final int statusCode) {
        if (isEnabled()) {
            LOGGER.info(String.format(
                    "|............... Response %d ............|\n%s\n",
                    statusCode, formatJson(json)));
        }
    }

    public static void logException(final Exception ex) {
        if (isEnabled()) {
            LOGGER.info(String.format(
                    "|............... %s ............|\n%s\n", ex.getClass()
                            .getSimpleName(), ex.getMessage()));
        }
    }

    /**
     * Pretty formats raw JSON input.
     *
     * @param input
     *            The JSON input.
     * @return The pretty formatted JSON.
     */
    private static String formatJson(final String input) {
        try {
            return JsonAbstractBase.prettyPrint(input);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return input;
        }
    }

}
