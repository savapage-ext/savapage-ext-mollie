/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.savapage.core.json.JsonAbstractBase;
import org.savapage.core.util.IOHelper;
import org.savapage.ext.payment.PaymentGatewayException;
import org.savapage.ext.payment.PaymentGatewayPlugin.PaymentRequest;
import org.savapage.ext.payment.mollie.json.MollieAmountV2;
import org.savapage.ext.payment.mollie.json.MollieErrorV1;
import org.savapage.ext.payment.mollie.json.MollieErrorV2;
import org.savapage.ext.payment.mollie.json.MolliePayment;
import org.savapage.ext.payment.mollie.json.MolliePaymentMethodsV1;
import org.savapage.ext.payment.mollie.json.MolliePaymentMethodsV2;
import org.savapage.ext.payment.mollie.json.MolliePaymentV1;
import org.savapage.ext.payment.mollie.json.MolliePaymentV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class MollieClient {

    /**
     * The {@link Logger}.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(MollieClient.class);

    /**
     * The Mollie Payment resource.
     */
    private static final String RESOURCE_PAYMENTS = "payments";

    /**
     * The Mollie Methods resource.
     */
    private static final String RESOURCE_METHODS = "methods";

    /**
     * .
     */
    private final MolliePlugin paymentPlugin;

    /**
     *
     * @param plugin
     *            The {@link MolliePlugin}.
     */
    public MollieClient(final MolliePlugin plugin) {
        this.paymentPlugin = plugin;
    }

    /**
     * Gets the active payment methods from Mollie.
     *
     * @return An {@link IMolliePaymentMethods} instance.
     * @throws IOException
     *             When communication errors.
     * @throws PaymentGatewayException
     *             When HTTP response status NEQ "200 OK".
     */
    public IMolliePaymentMethods getPaymentMethods()
            throws IOException, PaymentGatewayException {

        final URI uri = this.composeApiUri(RESOURCE_METHODS);

        final HttpGet httpRequest = new HttpGet(uri);
        setHeader(httpRequest);

        final StringBuilder responseBuilder = new StringBuilder();

        final int httpStatus = this.send(httpRequest, "", responseBuilder);

        final String jsonRsp = responseBuilder.toString();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(JsonAbstractBase.prettyPrint(jsonRsp));
        }

        final MolliePlugin.ApiVersionEnum apiVersion =
                this.paymentPlugin.getApiVersion();

        if (httpStatus == HttpStatus.SC_OK) {

            final IMolliePaymentMethods methods;

            if (apiVersion.equals(MolliePlugin.ApiVersionEnum.V2)) {
                methods = MolliePaymentMethodsV2.create(jsonRsp);
            } else {
                methods = MolliePaymentMethodsV1.create(jsonRsp);
            }
            return methods;
        }
        throw this.handleHttpError(uri, httpStatus, apiVersion, jsonRsp);
    }

    /**
     * Log HTTP error and create exception to be thrown.
     *
     * @param uri
     * @param httpStatus
     * @param apiVersion
     * @param jsonError
     * @return Exception to be thrown.
     * @throws IOException
     */
    private PaymentGatewayException handleHttpError(final URI uri,
            final int httpStatus, final MolliePlugin.ApiVersionEnum apiVersion,
            final String jsonError) throws IOException {

        final JsonAbstractBase jsonObj;

        if (apiVersion.equals(MolliePlugin.ApiVersionEnum.V2)) {
            jsonObj = MollieErrorV2.create(jsonError);
        } else {
            jsonObj = MollieErrorV1.create(jsonError);
        }

        final String msg = String.format("%s returns HTTP status %d",
                uri.toString(), httpStatus);

        LOGGER.error("{}.\n{}", msg, jsonObj.stringifyPrettyPrinted());

        return new PaymentGatewayException(msg);
    }

    /**
     * Gets the payment from Mollie.
     *
     * @param id
     *            The unique Mollie payment id.
     * @return The {@link MolliePayment}.
     * @throws IOException
     *             When communication errors.
     * @throws PaymentGatewayException
     *             When HTTP response status NEQ "200 OK".
     */
    public MolliePayment getPayment(final String id)
            throws IOException, PaymentGatewayException {

        final MolliePlugin.ApiVersionEnum apiVersion =
                this.paymentPlugin.getApiVersion();

        final URI uri =
                composeApiUri(String.format("%s/%s", RESOURCE_PAYMENTS, id));

        final HttpGet httpRequest = new HttpGet(uri);
        setHeader(httpRequest);

        final StringBuilder responseBuilder = new StringBuilder();
        final MolliePayment payment;

        final int httpStatus = this.send(httpRequest, "", responseBuilder);
        final String jsonRsp = responseBuilder.toString();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(JsonAbstractBase.prettyPrint(jsonRsp));
        }

        if (httpStatus == HttpStatus.SC_OK) {
            if (apiVersion.equals(MolliePlugin.ApiVersionEnum.V2)) {
                payment = MolliePaymentV2.create(jsonRsp);
            } else {
                payment = MolliePaymentV1.create(jsonRsp);
            }
        } else {
            throw new PaymentGatewayException(String.format(
                    "%s returns HTTP status %d", uri.toString(), httpStatus));
        }
        return payment;
    }

    /**
     * Posts a payment to Mollie.
     *
     * @param req
     *            The {@link PaymentRequest}. The unique user id.
     * @return The {@link MolliePayment}.
     * @throws IOException
     *             If communication errors.
     * @throws PaymentGatewayException
     *             If HTTP response status NEQ "201 Created".
     */
    public MolliePayment postPayment(final PaymentRequest req)
            throws IOException, PaymentGatewayException {

        final MolliePlugin.ApiVersionEnum apiVersion =
                this.paymentPlugin.getApiVersion();
        /*
         * Create input.
         */
        final MolliePayment paymentIn;

        if (apiVersion.equals(MolliePlugin.ApiVersionEnum.V2)) {

            final MollieAmountV2 amount = new MollieAmountV2();

            amount.setValue(String.valueOf(
                    String.format(Locale.ENGLISH, "%.02f", req.getAmount())));

            amount.setCurrency(MolliePlugin.CURRENCY_CODE_EURO); // TODO
            final MolliePaymentV2 obj = new MolliePaymentV2();
            obj.setAmount(amount);
            paymentIn = obj;

        } else {

            final MolliePaymentV1 obj = new MolliePaymentV1();
            obj.setAmount(req.getAmount());
            paymentIn = obj;

        }

        paymentIn.setDescription(req.getDescription());
        paymentIn.setWebhookUrl(req.getCallbackUrl().toExternalForm());
        paymentIn.setRedirectUrl(req.getRedirectUrl().toExternalForm());
        paymentIn.setMethod(
                this.paymentPlugin.asMolliePaymenMethod(req.getMethod()));

        final MolliePayment.MetaData metadata = new MolliePayment.MetaData();

        metadata.setUserId(req.getUserId());

        paymentIn.setMetadata(metadata);

        /*
         * Send request
         */
        final URI uri = composeApiUri(RESOURCE_PAYMENTS);
        final HttpPost httpRequest = new HttpPost(uri);

        setHeader(httpRequest);

        final String jsonIn = paymentIn.stringifyPrettyPrinted();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(JsonAbstractBase.prettyPrint(jsonIn));
        }

        final StringEntity entity = new StringEntity(jsonIn);

        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        httpRequest.setEntity(entity);

        final StringBuilder responseBuilder = new StringBuilder();
        final MolliePayment paymentOut;

        final int httpStatus = this.send(httpRequest, jsonIn, responseBuilder);
        final String jsonOut = responseBuilder.toString();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(JsonAbstractBase.prettyPrint(jsonOut));
        }

        if (httpStatus == HttpStatus.SC_CREATED) {

            if (apiVersion.equals(MolliePlugin.ApiVersionEnum.V2)) {
                paymentOut = MolliePaymentV2.create(jsonOut);
            } else {
                paymentOut = MolliePaymentV1.create(jsonOut);
            }
            return paymentOut;
        }

        throw this.handleHttpError(uri, httpStatus, apiVersion, jsonOut);
    }

    /**
     * Creates a threadsafe {@link CloseableHttpClient}.
     * <p>
     * Note: this method is for future use, when it is decided to use a
     * singleton {@link CloseableHttpClient}.
     * </p>
     *
     * @param maxConnections
     *            Maximum number of connections.
     * @return the client.
     */
    public static CloseableHttpClient buildClient(final int maxConnections) {

        final PoolingHttpClientConnectionManager connManager =
                new PoolingHttpClientConnectionManager();

        connManager.setDefaultMaxPerRoute(maxConnections);
        connManager.setMaxTotal(2 * maxConnections);

        final HttpClientBuilder builder =
                HttpClientBuilder.create().setConnectionManager(connManager);

        /*
         * While HttpClient instances are thread safe and can be shared between
         * multiple threads of execution, it is highly recommended that each
         * thread maintains its own dedicated instance of HttpContext.
         */
        return builder.build();
    }

    /**
     * Creates a {@link RequestConfig} with the configured connect and socket
     * timeout values.
     *
     * @return The {@link RequestConfig}.
     */
    private RequestConfig buildRequestConfig() {

        return RequestConfig.custom()
                .setConnectTimeout(this.paymentPlugin.getConnectTimeout())
                .setSocketTimeout(this.paymentPlugin.getSocketTimeout())
                .setConnectionRequestTimeout(
                        this.paymentPlugin.getSocketTimeout())
                .build();
    }

    /**
     * Sends an HTTP request, collects the response and returns the HTTP status
     * code.
     *
     * @param httpRequest
     *            The {@link HttpRequestBase}.
     * @param jsonIn
     *            The JSON input.
     * @param jsonOut
     *            The {@link StringBuilder} to append the JSON response to.
     * @return The HTTP status code.
     * @throws IOException
     *             When communication errors.
     */
    private int send(final HttpRequestBase httpRequest, final String jsonIn,
            final StringBuilder jsonOut) throws IOException {

        MollieLogger.logRequest(jsonIn, httpRequest.getURI());

        //
        final CloseableHttpClient httpClient = HttpClients.createMinimal();

        // Set timeout values
        httpRequest.setConfig(buildRequestConfig());

        final int statusCode;

        ByteArrayOutputStream bos;
        CloseableHttpResponse response = null;

        try {
            // Send it ...
            response = httpClient.execute(httpRequest);

            statusCode = response.getStatusLine().getStatusCode();

            bos = new ByteArrayOutputStream();
            response.getEntity().writeTo(bos);

            jsonOut.append(bos.toString());

            MollieLogger.logResponse(jsonOut.toString(), statusCode);

        } catch (IOException ex) {

            MollieLogger.logException(ex);
            throw ex;

        } finally {
            /*
             * Mantis #487: release the connection.
             */
            httpRequest.reset();
            //
            IOHelper.closeQuietly(response);
            IOHelper.closeQuietly(httpClient);
        }

        return statusCode;
    }

    /**
     * Sets the authorization in the header of the {@link HttpRequestBase}.
     *
     * @param httpRequest
     *            The {@link HttpRequestBase}.
     */
    private void setHeader(final HttpRequestBase httpRequest) {
        httpRequest.setHeader(HttpHeaders.AUTHORIZATION,
                String.format("Bearer %s", this.paymentPlugin.getApiKey()));
    }

    /**
     * Composes the API {@link URI}.
     *
     * @param apiPath
     *            The API path after the {@link #apiUrl}.
     * @return The composed API {@link URI}.
     */
    private URI composeApiUri(final String apiPath) {
        try {
            return new URI(String.format("%s/%s",
                    this.paymentPlugin.getApiUrl(), apiPath));
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

}
