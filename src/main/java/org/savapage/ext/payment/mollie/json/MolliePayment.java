/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie.json;

import org.savapage.core.json.JsonAbstractBase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author Rijk Ravestein
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class MolliePayment extends JsonAbstractBase {

    /**
     * Any data saved by Mollie alongside the payment. Whenever you fetching the
     * payment with Mollie API, metadata are included. You can use up to 1kB of
     * JSON.
     */
    public static class MetaData {

        private String userId;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }

    /**
     * An object with payment details. The exact details differ per payment
     * method.
     * <p>
     * Method "banktransfer" is not included.
     * </p>
     */
    @JsonInclude(Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Details {

        /**
         * Name of the consumer.
         * <p>
         * ideal, paypal
         * </p>
         */
        private String consumerName;

        /**
         * The consumer's IBAN bank account number (ideal) or emailaddress
         * (paypal).
         * <p>
         * ideal, paypal
         * </p>
         */
        private String consumerAccount;

        /**
         * The consumer's bank's BIC number.
         * <p>
         * ideal
         * </p>
         */
        private String consumerBic;

        /**
         * The reference the consumer should use for the wire transfer. Note
         * that you should not apply any formatting to the reference, you should
         * show it to the consumer as-is.
         */
        private String transferReference;

        /**
         * Name of the card holder.
         * <p>
         * creditcard
         * </p>
         */
        private String cardHolder;

        /**
         * The last 4 digits of the card number.
         * <p>
         * creditcard, mistercash
         * </p>
         */
        private String cardNumber;

        /**
         * Security type. Either normal or 3dsecure
         * <p>
         * creditcard
         * </p>
         */
        private String cardSecurity;

        /**
         * The bitcoinadres the bitcoins were transferred into.
         */
        private String bitcoinAddress;

        /**
         * The bitcoin amount that was transferred.
         * <p>
         * Deprecated: Removed in V2.
         * </p>
         */
        @Deprecated
        private String bitcoinAmount;

        /**
         * The used bitcoin to EURO exchange rate.
         * <p>
         * Deprecated: Removed in V2.
         * </p>
         */
        @Deprecated
        private String bitcoinRate;

        /**
         * A URI that's understood by bitcoin wallet clients and will cause such
         * clients to prepare the transaction.
         */
        private String bitcoinUri;

        /**
         * PayPal's reference to the transaction. For instance
         * 9AL35361CF606152E.
         */
        private String paypalReference;

        /**
         * The customerReference you supplied when creating the payment.
         * <p>
         * paysafecard
         * </p>
         */
        private String customerReference;

        /**
         *
         * @return
         */
        public String getConsumerName() {
            return consumerName;
        }

        public void setConsumerName(String consumerName) {
            this.consumerName = consumerName;
        }

        public String getConsumerAccount() {
            return consumerAccount;
        }

        public void setConsumerAccount(String consumerAccount) {
            this.consumerAccount = consumerAccount;
        }

        public String getConsumerBic() {
            return consumerBic;
        }

        public void setConsumerBic(String consumerBic) {
            this.consumerBic = consumerBic;
        }

        public String getCardHolder() {
            return cardHolder;
        }

        public void setCardHolder(String cardHolder) {
            this.cardHolder = cardHolder;
        }

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public String getCardSecurity() {
            return cardSecurity;
        }

        public void setCardSecurity(String cardSecurity) {
            this.cardSecurity = cardSecurity;
        }

        public String getBitcoinAddress() {
            return bitcoinAddress;
        }

        public void setBitcoinAddress(String bitcoinAddress) {
            this.bitcoinAddress = bitcoinAddress;
        }

        @Deprecated
        public String getBitcoinAmount() {
            return bitcoinAmount;
        }

        @Deprecated
        public void setBitcoinAmount(String bitcoinAmount) {
            this.bitcoinAmount = bitcoinAmount;
        }

        @Deprecated
        public String getBitcoinRate() {
            return bitcoinRate;
        }

        @Deprecated
        public void setBitcoinRate(String bitcoinRate) {
            this.bitcoinRate = bitcoinRate;
        }

        public String getBitcoinUri() {
            return bitcoinUri;
        }

        public void setBitcoinUri(String bitcoinUri) {
            this.bitcoinUri = bitcoinUri;
        }

        public String getPaypalReference() {
            return paypalReference;
        }

        public void setPaypalReference(String paypalReference) {
            this.paypalReference = paypalReference;
        }

        public String getCustomerReference() {
            return customerReference;
        }

        public void setCustomerReference(String customerReference) {
            this.customerReference = customerReference;
        }

        public String getTransferReference() {
            return transferReference;
        }

        public void setTransferReference(String transferReference) {
            this.transferReference = transferReference;
        }

    }

    private String id;
    private String mode;
    private String paidDatetime;
    private String status;
    private String method;
    private Details details;
    private String locale;

    private MetaData metadata;

    /**
     * The description for the payment you're creating. This will be shown to
     * the customer on their card or bank statement.
     */
    private String description;

    /**
     * The URL the customer will be redirected to after the payment process. It
     * could make sense for the redirectURL to contain a unique identifier –
     * like your order id – so you can show the right page referencing the order
     * when the customer returns.
     */
    private String redirectUrl;

    /**
     * Optional. Use this parameter to set a wehookURL for this payment only.
     * Mollie will ignore any webhook set in your website profile for this
     * payment.
     */
    private String webhookUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /** */
    public abstract String getCreatedDatetime();

    /** */
    public abstract void setCreatedDatetime(String createdDatetime);

    /** */
    public String getPaidDatetime() {
        return paidDatetime;
    }

    public void setPaidDatetime(String paidDatetime) {
        this.paidDatetime = paidDatetime;
    }

    public String getMode() {
        return mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public MetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(MetaData metadata) {
        this.metadata = metadata;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getWebhookUrl() {
        return webhookUrl;
    }

    public void setWebhookUrl(String webhookUrl) {
        this.webhookUrl = webhookUrl;
    }

    /**
     * @return Payment amount.
     */
    public abstract Double amountValue();

    public abstract String paymentUrlValue();

    /**
     * Creates an instance from a JSON string.
     *
     * @param json
     *            The JSON string.
     * @return The {@link MolliePayment} instance.
     */
    public static MolliePayment create(final String json) {
        return create(MolliePayment.class, json);
    }
}
