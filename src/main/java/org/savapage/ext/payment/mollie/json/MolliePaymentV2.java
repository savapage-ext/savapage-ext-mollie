/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie.json;

import org.savapage.core.json.JsonHypertextAppLang;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Rijk Ravestein
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class MolliePaymentV2 extends MolliePayment {

    @JsonProperty("createdAt")
    private String createdDatetime;

    private MollieAmountV2 amount;
    private Boolean isCancelable;
    private String expiresAt;

    @JsonProperty(JsonHypertextAppLang.PROP_LINKS)
    private MolliePaymentLinksV2 links;

    @Override
    public String getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public void setCreatedDatetime(final String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public MollieAmountV2 getAmount() {
        return amount;
    }

    public void setAmount(MollieAmountV2 amount) {
        this.amount = amount;
    }

    public Boolean getIsCancelable() {
        return isCancelable;
    }

    public void setIsCancelable(Boolean isCancelable) {
        this.isCancelable = isCancelable;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public MolliePaymentLinksV2 getLinks() {
        return links;
    }

    public void setLinks(MolliePaymentLinksV2 links) {
        this.links = links;
    }

    /**
     * Creates an instance from a JSON string.
     *
     * @param json
     *            The JSON string.
     * @return The {@link MolliePaymentV2} instance.
     */
    public static MolliePaymentV2 create(final String json) {
        return create(MolliePaymentV2.class, json);
    }

    @Override
    public Double amountValue() {
        if (this.amount == null) {
            return null;
        }
        return Double.parseDouble(this.amount.getValue());
    }

    @Override
    public String paymentUrlValue() {
        return this.links.getCheckout().getHref();
    }

}
