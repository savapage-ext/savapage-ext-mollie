/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.savapage.ext.ServerPluginContext;
import org.savapage.ext.payment.PaymentGateway;
import org.savapage.ext.payment.PaymentGatewayException;
import org.savapage.ext.payment.PaymentGatewayListener;
import org.savapage.ext.payment.PaymentGatewayTrx;
import org.savapage.ext.payment.PaymentGatewayTrxEvent;
import org.savapage.ext.payment.PaymentMethodEnum;
import org.savapage.ext.payment.PaymentMethodInfo;
import org.savapage.ext.payment.mollie.json.MolliePayment;
import org.savapage.ext.payment.mollie.json.MolliePayment.Details;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class MolliePlugin implements PaymentGateway {

    enum ApiVersionEnum {
        /** */
        @Deprecated
        V1("v1"),
        /** */
        V2("v2");

        /**
         * URL path suffix.
         */
        private final String suffix;

        ApiVersionEnum(final String sfx) {
            this.suffix = sfx;
        }

        /**
         * @return URL path suffix.
         */
        public String getApiSuffix() {
            return this.suffix;
        }

        static ApiVersionEnum getVersion(final String url) {
            if (url.endsWith(V1.getApiSuffix())) {
                return V1;
            }
            return V2;
        }
    };

    /**
     * The {@link Logger}.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(MolliePlugin.class);

    /**
     * URL parameter for testing by Mollie.
     */
    private static final String URL_PARM_TEST_BY_MOLLIE = "testByMollie";

    /**
     * .
     */
    public static final String CURRENCY_CODE_EURO = "EUR";

    /**
     * .
     */
    private static final String CURRENCY_CODE_BITCOIN = "BTC";

    /**
     * API property key prefix.
     */
    private static final String PROP_KEY_API_PFX = "mollie.api.";

    /**
     * Payment method property key prefix.
     */
    private static final String PROP_KEY_METHOD_PFX = "mollie.method.";

    /**
     * Property key for Web API URL. E.g. {@code https://api.mollie.nl/v1}
     */
    private static final String PROP_KEY_API_URL = PROP_KEY_API_PFX + "url";

    /**
     * .
     */
    private static final String PROP_KEY_API_KEY_TEST =
            PROP_KEY_API_PFX + "key.test";

    /**
     * .
     */
    private static final String PROP_KEY_API_KEY_LIVE =
            PROP_KEY_API_PFX + "key.live";

    /**
     * Timeout in milliseconds until a HTTP connection with server is
     * established.
     */
    private static final String PROP_KEY_API_TIMEOUT_CONNECT_MILLIS =
            PROP_KEY_API_PFX + "timeout.connect";

    /** */
    private static final String PROP_VALUE_API_TIMEOUT_CONNECT_MILLIS = "3000";

    /**
     * Timeout in milliseconds to receive data on established connection with
     * server, i.e. maximum time of inactivity between two data packets.
     */
    private static final String PROP_KEY_API_TIMEOUT_SOCKET_MILLIS =
            PROP_KEY_API_PFX + "timeout.socket";

    /** */
    private static final String PROP_VALUE_API_TIMEOUT_SOCKET_MILLIS = "3000";

    /**
     * .
     */
    private static final String CALLBACK_PARM_ID = "id";

    /**
     * .
     */
    private PaymentGatewayListener listener;

    /**
     * .
     */
    private Properties properties;

    /**
     *
     */
    private String id;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private boolean live;

    /**
    *
    */
    private boolean online = true;

    /**
     * Timeout in milliseconds until a HTTP connection with server is
     * established.
     */
    private int connectTimeout;

    /**
     * Timeout in milliseconds to receive data on established connection with
     * server, i.e. maximum time of inactivity between two data packets.
     */
    private int socketTimeout;

    /**
     *
     */
    private Map<PaymentMethodEnum, PaymentMethodInfo> paymentInfoMap = null;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isLive() {
        return this.live;
    }

    @Override
    public synchronized Map<PaymentMethodEnum, PaymentMethodInfo>
            getExternalPaymentMethods() throws PaymentGatewayException {

        if (this.paymentInfoMap == null) {

            try {
                this.paymentInfoMap = retrievePaymentMethodInfo();
            } catch (IOException e) {
                this.listener.onPluginException(this, e);
                /*
                 * Return empty map so clients will not be bothered with
                 * exceptions.
                 */
                return new HashMap<>();
            }
        }

        return this.paymentInfoMap;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    @Override
    public boolean isCurrencySupported(String currencyCode) {
        return currencyCode.equals(CURRENCY_CODE_EURO);
    }

    @Override
    public void onStart() throws PaymentGatewayException {
        this.getExternalPaymentMethods();
    }

    @Override
    public void onStop() {
        synchronized (this) {
            this.paymentInfoMap = null;
        }
    }

    @Override
    public PaymentGatewayTrx onPaymentRequest(final PaymentRequest req)
            throws IOException, PaymentGatewayException {

        /*
         * INVARIANT: currency code must be supported.
         */
        if (!this.isCurrencySupported(req.getCurrency().getCurrencyCode())) {
            throw new PaymentGatewayException(
                    String.format("Currency code %s is not supported.",
                            req.getCurrency().getCurrencyCode()));
        }

        /*
         * INVARIANT: user id must be specified.
         */
        if (StringUtils.isBlank(req.getUserId())) {
            throw new PaymentGatewayException("Userid not specified.");
        }

        final MolliePayment molliePayment =
                new MollieClient(this).postPayment(req);

        final PaymentGatewayTrx dto = new PaymentGatewayTrx();

        dto.setUserId(req.getUserId());
        dto.setAmount(BigDecimal.valueOf(req.getAmount()));
        dto.setTransactionId(molliePayment.getId());
        dto.setPaymentUrl(new URL(molliePayment.paymentUrlValue()));

        return dto;
    }

    @Override
    public CallbackResponse onCallBack(final Map<String, String[]> parameterMap,
            final boolean live, final Currency currency,
            final BufferedReader request, final PrintWriter response)
            throws IOException, PaymentGatewayException {

        MollieLogger.logCallback(parameterMap);

        //
        final CallbackResponse callbackResponse =
                new CallbackResponse(HttpStatus.SC_OK);

        /*
         */
        for (final Entry<String, String[]> entry : parameterMap.entrySet()) {
            if (entry.getKey().equalsIgnoreCase(URL_PARM_TEST_BY_MOLLIE)) {
                return callbackResponse;
            }
        }

        /*
         * INVARIANT: currency code must be supported.
         */
        if (!this.isCurrencySupported(currency.getCurrencyCode())) {
            throw new PaymentGatewayException(
                    String.format("Currency code %s is not supported.",
                            currency.getCurrencyCode()));
        }

        /*
         * This method is called when a Mollie payment's status changes, for
         * example from "open" to "paid". At this URL you should place a script
         * that – when it's called – fetches the payment status and processes
         * it, if it has changed. In case the status changed to "paid", you
         * should mark the order as paid.
         */

        /*
         * This method will be called with a single POST-parameter named "id"
         * which for instance will contain the value "tr_d0b0E3EA3v".
         */

        String paymentId = null;

        if (parameterMap.containsKey(CALLBACK_PARM_ID)) {
            final String[] values = parameterMap.get(CALLBACK_PARM_ID);
            if (values.length == 1) {
                paymentId = values[0];
            }
        }

        if (paymentId == null) {
            throw new PaymentGatewayException(String
                    .format("Parameter [%s] not found.", CALLBACK_PARM_ID));
        }

        /*
         * Use the "id" to actively fetch the payment to find out about it's
         * status. This step seems a little cumbersome but proper security
         * dictates this flow. Since the status is not transmitted in the
         * webhook, fake calls to your webhook will never result in orders being
         * processed without being actually paid.
         */
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Fetching status of payment [%s] ...",
                    paymentId));
        }

        final MolliePayment payment =
                new MollieClient(this).getPayment(paymentId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Payment [%s] status [%s]",
                    payment.getId(), payment.getStatus()));
        }

        /*
         * Create an independent DTO and pass to the listener.
         */
        final PaymentGatewayTrx trx = new PaymentGatewayTrx();

        trx.setLive(this.isLive());
        trx.setUserId(payment.getMetadata().getUserId());
        trx.setGatewayId(this.getId());
        trx.setTransactionId(paymentId);

        trx.setAmount(BigDecimal.valueOf(payment.amountValue()));
        trx.setExchangeCurrencyCode(currency.getCurrencyCode());
        trx.setExchangeRate(BigDecimal.ONE);

        trx.setCurrencyCode(CURRENCY_CODE_EURO);
        trx.setStatus(payment.getStatus());
        trx.setComment(payment.getDescription());
        trx.setConfirmations(1);

        final MolliePaymentStatusEnum paymentStat = MolliePaymentStatusEnum
                .valueOf(payment.getStatus().toUpperCase());

        //
        switch (paymentStat) {
        case CANCELED:
        case CANCELLED:
        case FAILED:
        case EXPIRED:
            LOGGER.warn("{} : {}", paymentId, paymentStat.name());
            break;
        default:
            this.setPaymentMethodDetails(payment, trx);
        }

        final PaymentGatewayTrxEvent trxEvent;

        switch (paymentStat) {

        case CANCELED:
            // No break intended.
        case CANCELLED:
            trxEvent = this.listener.onPaymentCancelled(trx);
            break;

        case EXPIRED:
            trxEvent = this.listener.onPaymentExpired(trx);
            break;

        case FAILED:
            trxEvent = this.listener.onPaymentFailed(trx);
            break;

        case PAID:
            trxEvent = this.listener.onPaymentAcknowledged(trx);
            break;

        case PENDING:
        case OPEN:
            trxEvent = this.listener.onPaymentPending(trx);
            break;

        case PAIDOUT:
            throw new IllegalStateException(String.format(
                    "Unexpected report of Mollie internal payment status [%s]",
                    paymentStat.toString().toLowerCase()));

        case REFUNDED:
            trxEvent = this.listener.onPaymentRefunded(trx);
            break;

        default:
            throw new UnsupportedOperationException(
                    String.format("Unknown Mollie payment status [%s]",
                            paymentStat.toString().toLowerCase()));
        }

        callbackResponse.setPluginObject(trxEvent);

        return callbackResponse;
    }

    @Override
    public void onCallBackCommitted(final Object pluginObject)
            throws PaymentGatewayException {

        if (pluginObject != null) {
            final PaymentGatewayTrxEvent event =
                    (PaymentGatewayTrxEvent) pluginObject;
            this.listener.onPaymentCommitted(event);
        }

    }

    /**
     * @param method
     * @return The fee.
     * @throws PaymentGatewayException
     */
    private double getFee(final PaymentMethodEnum method, final double amount)
            throws PaymentGatewayException {

        final PaymentMethodInfo info =
                this.getExternalPaymentMethods().get(method);

        double fee = 0.0;

        if (info.getFeeAmount() != null) {
            fee += info.getFeeAmount().doubleValue();
        }

        if (info.getFeePercentage() != null) {
            fee += info.getFeePercentage().doubleValue() * amount;
        }

        return fee;
    }

    /**
     *
     * @param method
     *            Payment method.
     * @return Method as {@link PaymentMethodEnum}.
     * @throws PaymentGatewayException
     *             When method is not supported.
     */
    private static PaymentMethodEnum asPaymenMethodEnum(final String method)
            throws PaymentGatewayException {

        switch (method) {

        case "ideal":
            return PaymentMethodEnum.IDEAL;
        case "creditcard":
            return PaymentMethodEnum.CREDITCARD;
        case "mistercash":
            return PaymentMethodEnum.MISTERCASH;
        case "belfius":
            return PaymentMethodEnum.BELFIUS;
        case "sofort":
            return PaymentMethodEnum.SOFORT;
        case "directdebit":
            return PaymentMethodEnum.DIRECTDEBIT;
        case "banktransfer":
            return PaymentMethodEnum.BANKTRANSFER;
        case "paypal":
            return PaymentMethodEnum.PAYPAL;
        case "bitcoin":
            return PaymentMethodEnum.BITCOIN;
        case "paysafecard":
            return PaymentMethodEnum.PAYSAFECARD;
        case "giropay":
            return PaymentMethodEnum.GIROPAY;
        case "kbc":
            return PaymentMethodEnum.KBC;
        case "inghomepay":
            return PaymentMethodEnum.INGHOMEPAY;
        default:
            throw new PaymentGatewayException(
                    String.format("Payment method [%s] is not supported.",
                            method.toString()));
        }
    }

    /**
     *
     * @param method
     *            Payment method.
     * @return Method as {@link PaymentMethodEnum}.
     * @throws PaymentGatewayException
     *             When method is not supported.
     */
    public String asMolliePaymenMethod(final PaymentMethodEnum method)
            throws PaymentGatewayException {

        switch (method) {

        case IDEAL:
            return "ideal";
        case CREDITCARD:
            return "creditcard";
        case MISTERCASH:
            return "mistercash";
        case BELFIUS:
            return "belfius";
        case SOFORT:
            return "sofort";
        case DIRECTDEBIT:
            return "directdebit";
        case BANKTRANSFER:
            return "banktransfer";
        case PAYPAL:
            return "paypal";
        case BITCOIN:
            return "bitcoin";
        case PAYSAFECARD:
            return "paysafecard";
        case GIROPAY:
            return "giropay";
        case KBC:
            return "kbc";
        case INGHOMEPAY:
            return "inghomepay";
        default:
            throw new PaymentGatewayException(
                    String.format("Payment method [%s] is not supported.",
                            method.toString()));
        }
    }

    /**
     *
     * @param payment
     * @param trx
     * @throws PaymentGatewayException
     */
    private void setPaymentMethodDetails(final MolliePayment payment,
            final PaymentGatewayTrx trx) throws PaymentGatewayException {

        final Details details = payment.getDetails();
        final PaymentMethodEnum method =
                asPaymenMethodEnum(payment.getMethod());

        final StringBuilder trxDetails = new StringBuilder();

        switch (method) {

        case IDEAL:
            trx.setTransactionAccount(String.format("%s %s",
                    details.getConsumerBic(), details.getConsumerAccount()));
            trxDetails.append(details.getConsumerName());
            break;

        case CREDITCARD:
            trx.setTransactionAccount(
                    String.format("***%s", details.getCardNumber()));
            trxDetails.append(details.getCardHolder());
            break;

        case MISTERCASH:
            trx.setTransactionAccount(details.getCardNumber());
            break;

        case BELFIUS:
            break;

        case SOFORT:
            break;

        case DIRECTDEBIT:
            trx.setTransactionAccount(String.format("%s %s",
                    details.getConsumerBic(), details.getConsumerAccount()));

            trxDetails.append(details.getConsumerName()).append(" | ")
                    .append(details.getTransferReference());
            break;

        case BANKTRANSFER:
            break;

        case PAYPAL:
            trx.setTransactionAccount(details.getPaypalReference());
            trxDetails.append(details.getConsumerName()).append(": ")
                    .append(details.getConsumerAccount());
            break;

        case BITCOIN:
            trx.setTransactionAccount(details.getBitcoinAddress());
            trxDetails.append(CURRENCY_CODE_BITCOIN).append(" ")
                    .append(details.getBitcoinAmount());
            break;

        case PAYSAFECARD:
            trxDetails.append(details.getCustomerReference());
            break;

        case GIROPAY:
        case KBC:
        case INGHOMEPAY:
            break;

        default:
            throw new PaymentGatewayException(String.format(
                    "Payment method [%s] is not handled", method.toString()));
        }

        if (trxDetails.length() > 0) {
            trx.setDetails(trxDetails.toString());
        }

        trx.setFee(
                BigDecimal.valueOf(this.getFee(method, payment.amountValue())));
        trx.setPaymentMethod(method);
    }

    /**
     *
     * @return The value of the API key.
     */
    public String getApiKey() {

        final String key;

        if (this.live) {
            key = PROP_KEY_API_KEY_LIVE;
        } else {
            key = PROP_KEY_API_KEY_TEST;
        }
        return this.properties.getProperty(key);
    }

    /**
     *
     * @return The value of {@link #PROP_KEY_API_URL}.
     */
    public String getApiUrl() {
        return this.properties.getProperty(PROP_KEY_API_URL);
    }

    /**
     * @return API version.
     */
    public ApiVersionEnum getApiVersion() {
        return ApiVersionEnum.getVersion(this.getApiUrl().toLowerCase());
    }

    @Override
    public void onInit(final String id, final String name, final boolean live,
            boolean online, final Properties props,
            final ServerPluginContext context) throws PaymentGatewayException {

        this.id = id;
        this.name = name;
        this.live = live;
        this.online = online;
        this.properties = props;

        this.connectTimeout = Integer
                .valueOf(props.getProperty(PROP_KEY_API_TIMEOUT_CONNECT_MILLIS,
                        PROP_VALUE_API_TIMEOUT_CONNECT_MILLIS))
                .intValue();

        this.socketTimeout = Integer
                .valueOf(props.getProperty(PROP_KEY_API_TIMEOUT_SOCKET_MILLIS,
                        PROP_VALUE_API_TIMEOUT_SOCKET_MILLIS))
                .intValue();
    }

    @Override
    public void onInit(final PaymentGatewayListener listener) {
        this.listener = listener;
    }

    /**
     *
     * @throws PaymentGatewayException
     * @throws IOException
     */
    private Map<PaymentMethodEnum, PaymentMethodInfo>
            retrievePaymentMethodInfo()
                    throws PaymentGatewayException, IOException {

        final Map<PaymentMethodEnum, PaymentMethodInfo> mapWrk =
                new HashMap<>();

        for (final PaymentMethodEnum method : PaymentMethodEnum.values()) {

            if (method == PaymentMethodEnum.OTHER) {
                continue;
            }

            final StringBuilder key =
                    new StringBuilder().append(PROP_KEY_METHOD_PFX)
                            .append(asMolliePaymenMethod(method));

            final String mollieMethod =
                    this.properties.getProperty(key.toString());

            if (mollieMethod == null) {
                continue;
            }

            final String[] feeArray = StringUtils.split(mollieMethod);

            if (feeArray.length == 0) {
                continue;
            }

            final PaymentMethodInfo info = new PaymentMethodInfo();

            info.setMethod(method);
            info.setFeeAmount(new BigDecimal(feeArray[0]));

            if (feeArray.length > 1) {
                info.setFeePercentage(new BigDecimal(feeArray[1]));
            }

            mapWrk.put(method, info);
        }

        if (mapWrk.isEmpty()) {
            return mapWrk;
        }

        /*
         * Retrieve the list of payment methods that are activated at Mollie for
         * our website profile API-key.
         */
        final IMolliePaymentMethods activeMethods =
                new MollieClient(this).getPaymentMethods();

        for (final IMolliePaymentMethods.MethodInfo method : activeMethods
                .createMethodInfo()) {

            final PaymentMethodInfo info;

            try {
                info = mapWrk.get(asPaymenMethodEnum(method.getId()));
            } catch (PaymentGatewayException e) {
                LOGGER.warn(e.getMessage());
                continue;
            }

            if (info == null) {
                continue;
            }

            if (method.getMinimumAmount() != null) {
                info.setMinAmount(BigDecimal
                        .valueOf(method.getMinimumAmount().doubleValue()));
            }
            if (method.getMaximumAmount() != null) {
                info.setMaxAmount(BigDecimal
                        .valueOf(method.getMaximumAmount().doubleValue()));
            }
        }

        /*
         * Remove map entries that are specified in property file but are not
         * active.
         */
        final Iterator<Entry<PaymentMethodEnum, PaymentMethodInfo>> iter =
                mapWrk.entrySet().iterator();

        while (iter.hasNext()) {
            final Entry<PaymentMethodEnum, PaymentMethodInfo> entry =
                    iter.next();
            if (entry.getValue().getMinAmount() == null) {
                iter.remove();
            }
        }

        return mapWrk;
    }

    @Override
    public boolean isOnline() {
        return this.online;
    }

    @Override
    public void setOnline(boolean online) {
        this.online = online;
    }
}
