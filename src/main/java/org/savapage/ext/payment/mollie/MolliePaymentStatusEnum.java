/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie;

/**
 *
 * Mollie payment status.
 * <p>
 * The enum string values are passed from/to Mollie in lower case.
 * <p>
 *
 * @author Rijk Ravestein
 *
 */
public enum MolliePaymentStatusEnum {

    /**
     * If the payment method supports captures, the payment method will have
     * this status for as long as new captures can be created.
     */
    AUTHORIZED,

    /**
     * The payment has been created, but nothing else has happened yet. This is
     * not a status Mollie will call our Webhook for.
     */
    OPEN,

    /**
     * User has cancelled the payment. This is a definitive status. Mollie will
     * call our webhook when this status is reached.
     */
    CANCELLED,
    /** */
    CANCELED,

    /**
     * The payment has expired. For some payment methods like banktransfer it
     * can take a few days for this status to occur. This status is definitive
     * and we will call our webhook when it occurs. How much time it takes for a
     * payment to expire depends on the payment method.
     */
    EXPIRED,

    /**
     * The payment has failed and cannot be completed with a different payment
     * method. We will call your webhook when a payment transitions to the
     * failed status.
     */
    FAILED,

    /**
     * This is a temporary status that can occur when the actual payment process
     * has been started, but it's not complete yet. Nothing really needs to
     * happen on our end when this status occurs. Mollie will not call our
     * webhook when this status occurs.
     */
    PENDING,

    /**
     * This status occurs whenever a payment is successfully paid. When this
     * status occurs our webhook is called.
     */
    PAID,

    /**
     * This status occurs for payments that Mollie transfered to our
     * bankaccount. Mollie don't call our webhook when this status occurs.
     */
    PAIDOUT,

    /**
     * When we issue a refund for a payment, it gets this status. When this
     * status occurs Mollie calls our webhook.
     */
    REFUNDED

}
