/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2024 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2024 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie.json;

import java.util.ArrayList;
import java.util.List;

import org.savapage.core.json.JsonHypertextAppLang;
import org.savapage.ext.payment.mollie.IMolliePaymentMethods;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author Rijk Ravestein
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ JsonHypertextAppLang.PROP_EMBEDDED,
        MolliePaymentMethodsV2.PROP_COUNT, JsonHypertextAppLang.PROP_LINKS })
public final class MolliePaymentMethodsV2 extends JsonHypertextAppLang
        implements IMolliePaymentMethods {

    /**
     * Embedded resources.
     */
    public static final String PROP_COUNT = "count";

    /**
     */
    public static class Image {

        private String size1x;
        private String size2x;
        private String svg;

        public String getSize1x() {
            return size1x;
        }

        public void setSize1x(String size1x) {
            this.size1x = size1x;
        }

        public String getSize2x() {
            return size2x;
        }

        public void setSize2x(String size2x) {
            this.size2x = size2x;
        }

        public String getSvg() {
            return svg;
        }

        public void setSvg(String svg) {
            this.svg = svg;
        }

    }

    /**
     */
    public static final class Method {

        /** */
        private String resource;
        private String id;
        private String description;
        private MollieAmountV2 minimumAmount;
        private MollieAmountV2 maximumAmount;
        private Image image;
        private String status;

        @JsonProperty(JsonHypertextAppLang.PROP_LINKS)
        private MollieLinksV2 links;

        public String getResource() {
            return resource;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public MollieAmountV2 getMinimumAmount() {
            return minimumAmount;
        }

        public void setMinimumAmount(MollieAmountV2 minimumAmount) {
            this.minimumAmount = minimumAmount;
        }

        public MollieAmountV2 getMaximumAmount() {
            return maximumAmount;
        }

        public void setMaximumAmount(MollieAmountV2 maximumAmount) {
            this.maximumAmount = maximumAmount;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public MollieLinksV2 getLinks() {
            return links;
        }

        public void setLinks(MollieLinksV2 links) {
            this.links = links;
        }

    }

    /**
     */
    public static class Embedded {

        private List<Method> methods;

        public List<Method> getMethods() {
            return methods;
        }

        public void setMethods(List<Method> methods) {
            this.methods = methods;
        }

    }

    /** */
    @JsonProperty(JsonHypertextAppLang.PROP_EMBEDDED)
    private Embedded embedded;

    @JsonProperty(PROP_COUNT)
    private Integer count;

    @JsonProperty(JsonHypertextAppLang.PROP_LINKS)
    private MollieLinksV2 links;

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public MollieLinksV2 getLinks() {
        return links;
    }

    public void setLinks(MollieLinksV2 links) {
        this.links = links;
    }

    /**
     * Creates an instance from a JSON string.
     *
     * @param json
     *            The JSON string.
     * @return The {@link MolliePaymentMethodsV2} instance.
     */
    public static MolliePaymentMethodsV2 create(final String json) {
        return create(MolliePaymentMethodsV2.class, json);
    }

    @Override
    public List<MethodInfo> createMethodInfo() {

        final List<MethodInfo> list = new ArrayList<>();

        for (final MolliePaymentMethodsV2.Method method : this.getEmbedded()
                .getMethods()) {

            final MethodInfo info = new MethodInfo();

            info.setId(method.getId());

            if (method.getMaximumAmount() != null) {
                info.setMaximumAmount(method.getMaximumAmount().amountValue());
            }
            if (method.getMinimumAmount() != null) {
                info.setMinimumAmount(method.getMinimumAmount().amountValue());
            }

            list.add(info);
        }
        return list;
    }

}
