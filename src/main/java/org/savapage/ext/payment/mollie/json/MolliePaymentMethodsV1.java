/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.payment.mollie.json;

import java.util.ArrayList;
import java.util.List;

import org.savapage.core.json.JsonAbstractBase;
import org.savapage.ext.payment.mollie.IMolliePaymentMethods;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author Rijk Ravestein
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Deprecated
public final class MolliePaymentMethodsV1 extends JsonAbstractBase
        implements IMolliePaymentMethods {

    /**
     */
    public static class Amount {

        private Double minimum;
        private Double maximum;

        public Double getMinimum() {
            return minimum;
        }

        public void setMinimum(Double minimum) {
            this.minimum = minimum;
        }

        public Double getMaximum() {
            return maximum;
        }

        public void setMaximum(Double maximum) {
            this.maximum = maximum;
        }

    }

    /**
     */
    public static class Image {

        private String normal;
        private String bigger;

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getBigger() {
            return bigger;
        }

        public void setBigger(String bigger) {
            this.bigger = bigger;
        }

    }

    /**
     */
    public static class Method {

        private String resource;
        private String id;
        private String description;
        private Amount amount;
        private Image image;

        public String getResource() {
            return resource;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Amount getAmount() {
            return amount;
        }

        public void setAmount(Amount amount) {
            this.amount = amount;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image image) {
            this.image = image;
        }

    }

    private Integer totalCount;
    private Integer offset;
    private Integer count;
    private List<Method> data;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Method> getData() {
        return data;
    }

    public void setData(List<Method> data) {
        this.data = data;
    }

    /**
     * Creates an instance from a JSON string.
     *
     * @param json
     *            The JSON string.
     * @return The {@link MolliePaymentMethodsV1} instance.
     */
    public static MolliePaymentMethodsV1 create(final String json) {
        return create(MolliePaymentMethodsV1.class, json);
    }

    @Override
    public List<MethodInfo> createMethodInfo() {

        final List<MethodInfo> list = new ArrayList<>();

        for (final MolliePaymentMethodsV1.Method method : this.getData()) {

            final MethodInfo info = new MethodInfo();

            info.setId(method.getId());
            info.setMaximumAmount(method.getAmount().getMaximum());
            info.setMinimumAmount(method.getAmount().getMinimum());

            list.add(info);
        }
        return list;
    }
}
