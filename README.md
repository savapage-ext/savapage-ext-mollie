<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

# savapage-ext-mollie
    
Payment Gateway Plug-in using [Mollie](https://www.mollie.nl) online payment services for: 

* Creditcard
* PayPal
* paysafecard
* SOFORT Banking (Europe)
* SEPA bank transfers (Europe)
* Bancontact/Mister Cash (Belgium)
* Belfius Direct Net (Belgium)
* IDEAL (Netherlands)
 
_Mollie supports EUR payments only._
 
### License

This module is part of the SavaPage project <https://www.savapage.org>,
copyright (c) 2011-2020 Datraverse B.V. and licensed under the
[GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl.html)
version 3, or (at your option) any later version.

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Members. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Usage

Copy plug-in to server environment:

    $ sudo cp savapage-ext-mollie.properties /opt/savapage/server/ext
    $ sudo target/savapage-ext-mollie.jar /opt/savapage/server/ext/lib

Set ownership and restrict permissions, since properties file contains confidential information:

    $ sudo chown savapage:savapage /opt/savapage/server/ext/savapage-ext-mollie.properties
    $ sudo chmod 600 /opt/savapage/server/ext/savapage-ext-mollie.properties
    
Edit `savapage-ext-mollie.properties` to specify your account and callback parameters.
    
Edit `/opt/savapage/server/lib/log4j.properties` and add:
    
    #------------------------------------------------------------------------------
    # Mollie Payment Gateway Plugin
    #------------------------------------------------------------------------------
    log4j.appender.ext_mollie=org.apache.log4j.RollingFileAppender
    log4j.appender.ext_mollie.MaxFileSize=10MB
    log4j.appender.ext_mollie.MaxBackupIndex=10
    log4j.appender.ext_mollie.File=${server.home}/ext/logs/mollie.log
    log4j.appender.ext_mollie.layout=org.apache.log4j.PatternLayout
    log4j.appender.ext_mollie.layout.ConversionPattern=%d{ISO8601} %m\n
    log4j.appender.ext_mollie.encoding=UTF8
    
    # Use INFO to activate the MollieLogger
    log4j.logger.org.savapage.ext.payment.mollie.MollieLogger=TRACE, ext_mollie
    log4j.additivity.org.savapage.ext.payment.mollie.MollieLogger=false

Make sure `/opt/savapage/server/lib/log4j.properties` contains a logger for the  Payment Gateway Audit Trail as explained in the `README.md` of the `savapage-ext` project.

Restart SavaPage.
                   

